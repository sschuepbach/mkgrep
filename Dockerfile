FROM frolvlad/alpine-glibc:alpine-3.12_glibc-2.32
WORKDIR /app
ENTRYPOINT ["/app/app"]
CMD ["-c", "/app/config.toml"]
ADD target/release/mkt /app/app
ADD configs/config.example.toml /app/config.toml
