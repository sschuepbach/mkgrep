use crate::config::Config;
use anyhow::{Context, Result};
use log::warn;
use rdkafka::config::ClientConfig;
use rdkafka::consumer::base_consumer::BaseConsumer;
use rdkafka::consumer::Consumer;
use rdkafka::error::KafkaResult;
use rdkafka::message::{BorrowedMessage, Message};
use regex::Regex;
use std::time::Duration;
use std::u32;

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum MatchSpace {
    OnKey,
    OnPayload,
    OnBoth,
}

#[derive(Debug)]
pub enum Operator {
    And,
    Or,
}

#[derive(Debug, PartialEq)]
pub struct Pattern {
    pub pattern: String,
    pub match_space: MatchSpace,
    pub inverse: bool,
}

impl Pattern {
    pub fn new(pattern: String, match_space: MatchSpace, inverse: bool) -> Pattern {
        Pattern {
            pattern,
            match_space,
            inverse,
        }
    }
}

pub type MessageFilter = Box<dyn Fn(&(Option<String>, Option<String>)) -> bool>;

pub fn fetch_all_messages(
    topic: &str,
    consumer: &BaseConsumer,
    timeout_in_ms: u64,
    patterns: Vec<Pattern>,
    operator: Operator,
) -> Result<()> {
    consumer
        .subscribe(&[topic])
        .context(format!("Couldn't subscribe to topic {}", topic))?;
    let match_fun = patterns.into_iter().map(create_filter).collect::<Vec<MessageFilter>>();
    let match_fun = match_fun.as_slice();
    while let Some(res) = consumer.poll(get_duration(timeout_in_ms)) {
        print_message(res, match_fun, &operator).context("Printing message failed")?
    }
    Ok(())
}

fn print_message(
    kafka_result: KafkaResult<BorrowedMessage>,
    match_fun: &[MessageFilter],
    operator: &Operator,
) -> Result<()> {
    if let Ok(msg) = kafka_result {
        let key = msg.key().and_then(|k| String::from_utf8(k.to_vec()).ok());
        let value = msg
            .payload()
            .and_then(|k| String::from_utf8(k.to_vec()).ok());
        if let Some(line) = apply_filters((key, value), match_fun, operator) {
            println!(
                "{}\t{}",
                line.0.as_ref().unwrap_or(&"".to_string()),
                line.1.as_ref().unwrap_or(&"".to_string())
            );
        }
    } else {
        warn!("Reading message failed!")
    }
    Ok(())
}

pub fn list_topics(consumer: &BaseConsumer, timeout_in_ms: u64) -> Result<()> {
    let metadata = consumer
        .fetch_metadata(None, get_duration(timeout_in_ms))
        .context("Fetching of cluster metadata failed!")?;
    for topic_metadata in metadata.topics() {
        println!("{}", topic_metadata.name());
    }
    Ok(())
}

fn get_duration(timeout_in_ms: u64) -> Duration {
    let secs = timeout_in_ms / 1000;
    let millisecs = timeout_in_ms % 1000;
    Duration::new(secs, (millisecs as u32) * 1000000)
}

fn apply_filters(
    msg: (Option<String>, Option<String>),
    filters: &[MessageFilter],
    operator: &Operator,
) -> Option<(Option<String>, Option<String>)> {
    let condition = match operator {
        Operator::And => filters.iter().all(|filter| filter(&msg)),
        Operator::Or => filters.iter().any(|filter| filter(&msg)),
    };
    if condition {
        Some(msg)
    } else {
        None
    }
}

fn create_filter(pattern: Pattern) -> MessageFilter {
    let re: Regex = Regex::new(&pattern.pattern).unwrap();
    Box::new(move |(key, value)| match pattern.match_space {
        MatchSpace::OnKey => pattern.inverse ^ matches(&re, key),
        MatchSpace::OnPayload => pattern.inverse ^ matches(&re, value),
        MatchSpace::OnBoth => pattern.inverse ^ (matches(&re, key) || matches(&re, value)),
    })
}

fn matches(re: &Regex, opt_string: &Option<String>) -> bool {
    opt_string.is_some() && re.is_match(opt_string.as_ref().unwrap())
}

pub fn create_base_consumer(config: &Config) -> Result<BaseConsumer> {
    ClientConfig::new()
        .set("bootstrap.servers", &config.kafka.bootstrap_servers)
        .set("group.id", &config.kafka.group_id)
        .set("auto.offset.reset", "earliest")
        .set("allow.auto.create.topics", "false")
        //.set("exclude.internal.topic", "true")
        .set("enable.auto.commit", "false")
        //.set("max.poll.interval.ms", "2000")
        .set("request.timeout.ms", "5000")
        .set("check.crcs", "false")
        //.set("fetch.max.wait.ms", "100")
        .create()
        .context("Building of Kafka ClientConfig failed")
}

#[cfg(test)]
mod tests {
    use crate::kafka::{apply_filters, create_filter, get_duration, MatchSpace, Operator, Pattern};
    use std::time::Duration;

    fn generate_patterns() -> Vec<Pattern> {
        let match_spaces = &[MatchSpace::OnBoth, MatchSpace::OnKey, MatchSpace::OnPayload];
        let inverse = &[true, false];
        let patterns = &["a_ma.*".to_string(), "not_a_ma.*".to_string()];
        let mut v = vec![];
        for p in patterns {
            for ms in match_spaces {
                for i in inverse {
                    v.push(Pattern::new(p.clone(), *ms, *i));
                }
            }
        }
        v
    }

    #[test]
    fn test_get_duration() {
        let result = Duration::new(2, 500 * 1000 * 1000);
        assert_eq!(get_duration(2500), result);
    }

    #[test]
    fn test_apply_filters<'a>() {
        let msg = (
            Some(String::from("a_match")),
            Some(String::from("some_payload")),
        );
        let patterns = generate_patterns();
        let results = vec![
            None,
            Some(msg.clone()),
            None,
            Some(msg.clone()),
            Some(msg.clone()),
            None,
            Some(msg.clone()),
            None,
            Some(msg.clone()),
            None,
            Some(msg.clone()),
            None,
        ];
        for (pattern, result) in patterns.into_iter().zip(results) {
            // dbg!(&pattern.pattern, &pattern.inverse, &pattern.match_space);
            let filter = create_filter(pattern);
            assert_eq!(
                apply_filters(msg.clone(), &vec![filter], &Operator::And),
                result
            );
        }
    }

    #[test]
    fn test_apply_filters_none_values() {
        let msg_without_key = (None, Some(String::from("some_payload")));
        let msg_without_payload = (Some(String::from("a_match")), None);
        let should_match_key =
            create_filter(Pattern::new(".*tch$".to_string(), MatchSpace::OnKey, true));
        let should_not_match_key = create_filter(Pattern::new(
            ".*tch$".to_string(),
            MatchSpace::OnBoth,
            false,
        ));
        let should_match_payload = create_filter(Pattern::new(
            ".*pay.*".to_string(),
            MatchSpace::OnPayload,
            true,
        ));
        let should_not_match_payload = create_filter(Pattern::new(
            ".*pay.*".to_string(),
            MatchSpace::OnPayload,
            false,
        ));
        assert_eq!(
            apply_filters(
                msg_without_key.clone(),
                &vec![should_match_key],
                &Operator::And,
            ),
            Some(msg_without_key.clone())
        );
        assert_eq!(
            apply_filters(
                msg_without_key.clone(),
                &vec![should_not_match_key],
                &Operator::And,
            ),
            None
        );
        assert_eq!(
            apply_filters(
                msg_without_payload.clone(),
                &vec![should_match_payload],
                &Operator::And,
            ),
            Some(msg_without_payload.clone())
        );
        assert_eq!(
            apply_filters(
                msg_without_payload.clone(),
                &vec![should_not_match_payload],
                &Operator::And,
            ),
            None
        );
    }

    #[test]
    fn test_apply_filters_chained_and() {
        let msg = (
            Some(String::from("a_match")),
            Some(String::from("some_payload")),
        );
        let should_match_key = create_filter(Pattern::new(
            ".*tch$".to_string(),
            MatchSpace::OnBoth,
            false,
        ));
        let should_match_payload = create_filter(Pattern::new(
            ".*pay.*".to_string(),
            MatchSpace::OnPayload,
            false,
        ));
        assert_eq!(
            apply_filters(
                msg.clone(),
                &vec![should_match_key, should_match_payload],
                &Operator::And,
            ),
            Some(msg)
        );
    }

    #[test]
    fn test_apply_filters_chained_or() {
        let msg = (
            Some(String::from("a_match")),
            Some(String::from("some_payload")),
        );
        let should_not_match_key =
            create_filter(Pattern::new(".*tch$".to_string(), MatchSpace::OnBoth, true));
        let should_match_payload = create_filter(Pattern::new(
            ".*pay.*".to_string(),
            MatchSpace::OnPayload,
            false,
        ));
        assert_eq!(
            apply_filters(
                msg.clone(),
                &vec![should_not_match_key, should_match_payload],
                &Operator::Or,
            ),
            Some(msg)
        );
    }
}
