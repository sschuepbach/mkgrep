use crate::kafka::{MatchSpace, Pattern};

use anyhow::{Context, Result};
use clap::{App, Arg, ArgMatches, SubCommand};
use serde::Deserialize;
use std::fs::File;
use std::io::prelude::*;

fn create_arg_parser<'a>() -> App<'a, 'static> {
    App::new("Memobase Kafka Topics")
        .version("0.0.1")
        .author("Sebastian Schüpbach <sebastian.schuepbach@unibas.ch>")
        .about("Explore data in the Memobase Kafka cluster")
        .arg(
            Arg::with_name("config")
                .short("c")
                .long("config")
                .value_name("FILE")
                .help("Sets a custom config file")
                .takes_value(true),
        )
        .subcommand(SubCommand::with_name("list").about("List all topics in cluster"))
        .subcommand(
            SubCommand::with_name("read")
                .about("Read from a specific topic")
                .arg(
                    Arg::with_name("TOPIC")
                        .required(true)
                        .index(1)
                        .help("Topic to read from"),
                )
                .arg(
                    Arg::with_name("filter")
                        .short("f")
                        .long("filter")
                        .alias("filter-both")
                        .value_name("PATTERN")
                        .multiple(true)
                        .help("Show message if key or payload matches pattern"),
                )
                .arg(
                    Arg::with_name("filter_key")
                        .short("k")
                        .long("filter-key")
                        .value_name("PATTERN")
                        .multiple(true)
                        .help("Show message if key matches pattern"),
                )
                .arg(
                    Arg::with_name("filter_payload")
                        .short("p")
                        .long("filter-payload")
                        .value_name("PATTERN")
                        .multiple(true)
                        .help("Show message if payload matches pattern"),
                )
                .arg(
                    Arg::with_name("filter_not")
                        .short("F")
                        .long("filter-not")
                        .alias("filter-both-not")
                        .value_name("PATTERN")
                        .multiple(true)
                        .help("Show message if key or payload don't match pattern"),
                )
                .arg(
                    Arg::with_name("filter_not_key")
                        .short("K")
                        .long("filter-not-key")
                        .value_name("PATTERN")
                        .multiple(true)
                        .help("Show message if key doesn't match pattern"),
                )
                .arg(
                    Arg::with_name("filter_not_payload")
                        .short("P")
                        .long("filter-not-payload")
                        .value_name("PATTERN")
                        .multiple(true)
                        .help("Show message if payload doesn't match pattern"),
                )
                .arg(
                    Arg::with_name("connector")
                        .short("c")
                        .long("connector")
                        .default_value("AND")
                        .possible_values(&["AND", "OR"])
                        .help("How the filters are connected"),
                ),
        )
}

pub fn parse_args() -> ArgMatches<'static> {
    create_arg_parser().get_matches()
}

pub fn parse_config_file(path: &str) -> Result<Config> {
    let mut file = File::open(path)?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    toml::from_str(&contents).context("Parsing of TOML config file failed")
}

pub fn extract_filters(arg_matches: &ArgMatches) -> Vec<Pattern> {
    let filter = create_patterns(arg_matches, "filter", MatchSpace::OnBoth, false);
    let filter_not = create_patterns(arg_matches, "filter_not", MatchSpace::OnBoth, true);
    let filter_key = create_patterns(arg_matches, "filter_key", MatchSpace::OnKey, false);
    let filter_not_key = create_patterns(arg_matches, "filter_not_key", MatchSpace::OnKey, true);
    let filter_payload =
        create_patterns(arg_matches, "filter_payload", MatchSpace::OnPayload, false);
    let filter_not_payload = create_patterns(
        arg_matches,
        "filter_not_payload",
        MatchSpace::OnPayload,
        true,
    );
    filter
        .unwrap_or_default()
        .into_iter()
        .chain(filter_not.unwrap_or_default().into_iter())
        .chain(filter_key.unwrap_or_default().into_iter())
        .chain(filter_not_key.unwrap_or_default().into_iter())
        .chain(filter_payload.unwrap_or_default().into_iter())
        .chain(filter_not_payload.unwrap_or_default().into_iter())
        .collect()
}

fn create_patterns<'a>(
    arg_matches: &'a ArgMatches,
    name: &str,
    match_space: MatchSpace,
    inverse: bool,
) -> Option<Vec<Pattern>> {
    arg_matches.values_of(name).map(|values| {
        values
            .map(|value| Pattern::new(value.to_owned(), match_space, inverse))
            .collect::<Vec<Pattern>>()
    })
}

#[derive(Deserialize)]
pub struct Config {
    pub kafka: KafkaConfig,
}

#[derive(Deserialize)]
pub struct KafkaConfig {
    pub bootstrap_servers: String,
    pub client_id: Option<String>,
    pub key_deserializer: Option<String>,
    pub value_deserializer: Option<String>,
    pub group_id: String,
    pub poll_timeout_ms: u64,
}

#[cfg(test)]
mod tests {
    use crate::config::{create_arg_parser, extract_filters};
    use crate::kafka::{MatchSpace, Pattern};

    #[test]
    fn test_extract_filters() {
        let matches = &[
            "mkt", "-c", "a_config", "read", "a_topic", "-f", "pat1", "-f", "pat2", "-P", "pat3",
            "-p", "pat4", "-F", "pat5", "-k", "pat6", "-F", "pat7", "-K", "pat8",
        ];
        let expected_result = vec![
            Pattern::new("pat1".to_string(), MatchSpace::OnBoth, false),
            Pattern::new("pat2".to_string(), MatchSpace::OnBoth, false),
            Pattern::new("pat5".to_string(), MatchSpace::OnBoth, true),
            Pattern::new("pat7".to_string(), MatchSpace::OnBoth, true),
            Pattern::new("pat6".to_string(), MatchSpace::OnKey, false),
            Pattern::new("pat8".to_string(), MatchSpace::OnKey, true),
            Pattern::new("pat4".to_string(), MatchSpace::OnPayload, false),
            Pattern::new("pat3".to_string(), MatchSpace::OnPayload, true),
        ];
        let arg_matches = create_arg_parser().get_matches_from(matches);
        let patterns = extract_filters(&(arg_matches.subcommand_matches("read").unwrap()));
        assert_eq!(patterns, expected_result);
    }
}
