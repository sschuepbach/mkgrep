mod config;
mod kafka;

use anyhow::{Context, Result};

fn main() -> Result<()> {
    env_logger::init();
    let matches = config::parse_args();

    let config_path = matches.value_of("config").unwrap_or("config.toml");
    let config_content = config::parse_config_file(&config_path);
    let config = config_content.context("Can't parse config file")?;
    if let Some(matches) = matches.subcommand_matches("read") {
        let cc = kafka::create_base_consumer(&config).context("Can't build base consumer!")?;
        let filters = config::extract_filters(matches);
        let operator = match matches.value_of("connector").unwrap() {
            "OR" => kafka::Operator::Or,
            _ => kafka::Operator::And,
        };
        kafka::fetch_all_messages(
            matches.value_of("TOPIC").unwrap(),
            &cc,
            config.kafka.poll_timeout_ms,
            filters,
            operator,
        )
        .context("Fetching messages failed!")?;
    }
    if matches.subcommand_matches("list").is_some() {
        let cc = kafka::create_base_consumer(&config).context("Can't build base consumer!")?;
        kafka::list_topics(&cc, config.kafka.poll_timeout_ms).context("Listing topics failed!")?;
    }
    Ok(())
}
